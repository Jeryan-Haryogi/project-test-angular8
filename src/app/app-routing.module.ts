import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AboutComponent} from './about/about.component'
import {AppComponent} from './app.component';
import {FaqComponent} from './faq/faq.component';
import {NotfoundComponent} from './notfound/notfound.component';
import {LoginComponent} from './login/login.component';
import {BerandaComponent} from './beranda/beranda.component';
import {DahsboardComponent} from './dahsboard/dahsboard.component'
import { HomeComponent } from "./home/home.component";
import { UserComponent } from "./user/user.component";
import {PropertiComponent} from "./properti/properti.component"
import { PemesananComponent } from "./pemesanan/pemesanan.component";
const routes: Routes = [
  {
    path: '', 
    redirectTo: '/beranda',
     pathMatch: 'full',
  },
  {
    path: 'beranda',
    component: BerandaComponent
  },
  {
    path: 'user/pesanan',
    component: PemesananComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'user/home',
    component: UserComponent
  },
  {
    path: 'admin/home',
    component: HomeComponent
  }, 
  {
    path: 'user/Favorit',
    component: PropertiComponent
  },
  {
    path: 'faq',
    component: FaqComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '**',
    component: NotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
