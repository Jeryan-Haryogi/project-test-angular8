import { Component, OnInit } from '@angular/core';
import { Title }     from '@angular/platform-browser';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  lorem = 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nostrum voluptates mollitia aspernatur numquam doloremque pariatur vitae odio laudantium, quisquam nihil tenetur at fugiat deleniti ea amet quod dolor, facere inventore';
  constructor(private titleService: Title ) { }
  public setTitle( newTitle: string) {
    this.titleService.setTitle(newTitle );
  }
  ngOnInit(): void {
  }

}
