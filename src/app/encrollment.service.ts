import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { from } from 'rxjs';
import {Login} from './login.module';

@Injectable({
  providedIn: 'root'
})
export class EncrollmentService {
  uri = 'assets/data/login.json';
  constructor(private _http: HttpClient) { }
 getLogin()
 {
   return this._http.get<Login[]>(this.uri);
 }
}
