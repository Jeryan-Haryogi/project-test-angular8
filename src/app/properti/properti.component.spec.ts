import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertiComponent } from './properti.component';

describe('PropertiComponent', () => {
  let component: PropertiComponent;
  let fixture: ComponentFixture<PropertiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
