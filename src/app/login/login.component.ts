import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {EncrollmentService} from '../encrollment.service';
import {Login} from '../login.module';
import { NULL_EXPR } from '@angular/compiler/src/output/output_ast';
import { from } from 'rxjs';
import { Title }     from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  angForm: FormGroup;
  constructor(private fb: FormBuilder,private route:ActivatedRoute,private router:Router, private encrollmentService:EncrollmentService, private titleService: Title ) {
    this.createForm();
  }
  public setTitle( newTitle: string) {
    this.titleService.setTitle(newTitle );
  }
  createForm() {
    this.angForm = this.fb.group({
      LoginEmail: ['', Validators.required ],
      LoginPassword: ['', Validators.required ]
    });
  }

    Login1: Login[];
  ngOnInit() {
    return this.encrollmentService.getLogin().subscribe(data => this.Login1 = data);
  }
  LoginUser(e)
  {
    e.preventDefault();
    // console.log(e);
    var email = e.target.elements[0].value;
    var password = e.target.elements[1].value
    var validEmail = 'jeryan@gmail.com';
    var  validPassword = 'admin';
    
  
   if (email == null && password == null)
  {
    alert('Email Dan Password Belom Anda Isi');
  }
    else if(email == validEmail && password == validPassword )
    {
      this.router.navigate(['admin/home']);
    }else if(email == 'user@gmail.com' && password == 'user' )
    {
      this.router.navigate(['user/home']);
    }else if (email != 'user@gmail.com') {
      alert('Email Yang Anda Masukan Salah');
    }else if (password != 'user') {
      alert('Password Yang Anda Masukan Salah');
    }
    else if (email != validEmail)
    {
      alert('Email Yang Anda Masukan Salah');
    
  }else if(password != validPassword)
  {
    alert('Pasword Yang Anda Masukan Salah');
  }
  else {
    alert('Email Dan Password Yang Anda Masukan Salah');
  }
  }
}
