import { BrowserModule, Title} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { FaqComponent } from './faq/faq.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import {DahsboardComponent} from './dahsboard/dahsboard.component';
import {UserService} from './user/user.service';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    FaqComponent,
    NotfoundComponent,
    LoginComponent,
    DahsboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [Title, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
