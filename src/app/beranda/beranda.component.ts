import { Component, OnInit } from '@angular/core';
import { Title }     from '@angular/platform-browser';
@Component({
  selector: 'app-beranda',
  templateUrl: './beranda.component.html',
  styleUrls: ['./beranda.component.css']
})
export class BerandaComponent implements OnInit {

  constructor(private titleService: Title) { }
  public setTitle( newTitle: string) {
    this.titleService.setTitle(newTitle );
  }
  ngOnInit(): void {
  }

}
