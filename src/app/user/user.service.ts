import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { from } from 'rxjs';
import {HotelDataAPI} from './user.module';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = 'https://jsonplaceholder.typicode.com/users';
  constructor(private _hhtp: HttpClient) { }
  getUserAPI()
  {
    return this._hhtp.get<HotelDataAPI[]>(this.url);
  }
}
