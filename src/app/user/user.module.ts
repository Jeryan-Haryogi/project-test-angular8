export class HotelDataAPI {
    id: number;
    name: string;
    username: string;
    email: string;
}