import { Component, OnInit } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import {UserService} from './user.service';
import {HotelDataAPI} from './user.module';
import { from } from 'rxjs';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private titleService: Title, private userService: UserService) { }
  public setTitle( newTitle: string) {
    this.titleService.setTitle(newTitle );
  }
  HotelData: HotelDataAPI[];
  ngOnInit() {
    return this.userService.getUserAPI().subscribe(data => {this.HotelData = data});

  }

}
