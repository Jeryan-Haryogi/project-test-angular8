import { TestBed } from '@angular/core/testing';

import { EncrollmentService } from './encrollment.service';

describe('EncrollmentService', () => {
  let service: EncrollmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EncrollmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
